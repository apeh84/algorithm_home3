/*
REF:
https://git.wut.ee/i231/home3/src/master/description.html
*/

import java.util.Iterator;
import java.util.LinkedList;

public class LongStack {

   private final LinkedList<Long> plateRack;

   public static void main (String[] argum) {
      long i = interpret("2 15 -");
      System.out.println(i);
   }

   public LongStack() {
      this.plateRack = new LinkedList<>();
   }

   LongStack(LinkedList<Long> lifo) {
      this.plateRack = lifo;
   }



   @Override
   public Object clone() throws CloneNotSupportedException {
      return new LongStack((LinkedList<Long>) this.plateRack.clone());
   }

   public boolean stEmpty() {
      return this.plateRack.isEmpty();
   }

   public void push (long a) {
      this.plateRack.push(a);
   }

   public long pop() {
      return this.plateRack.pop();
   }

   public void op (String s) {
      if (s.equals("+")) {
         this.plateRack.push(this.plateRack.pop() + this.plateRack.pop());
      } else if (s.equals("-")) {
         long r1 = this.pop();
         long r2 = this.pop();
         this.plateRack.push(r2 - r1);
      } else if (s.equals("*")) {
         this.plateRack.push(this.plateRack.pop() * this.plateRack.pop());
      } else if (s.equals("/")) {
         long r1 = this.pop();
         long r2 = this.pop();
         this.plateRack.push(r2 / r1);
      } else if(s.equals(" ") | s.equals("\t")) {
         return;
      } else {
         throw new RuntimeException("Invalid operation");
      }
   }

   public long tos() {
      return this.plateRack.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      return this.plateRack.equals(((LongStack)o).plateRack);
   }

   @Override
   public String toString() {
      StringBuilder s = new StringBuilder();
      Iterator<Long> i = this.plateRack.descendingIterator();
      while (i.hasNext()) {
         s.append(i.next());
         s.append(" ");
      }
      return s.toString();
   }

   public static long interpret (String pol) {
      LongStack lStack = new LongStack();
      for(int i = 0; i < pol.length(); i++) {
         char ch = pol.charAt(i);
         char ec = ' ';
         if (i+1 < pol.length()) {
            ec = pol.charAt(i + 1);
         }
         if ((ch == '-' & ec >= '0' & ec <= '9')| (ch >= '0' & ch <= '9')) {
            StringBuilder buf = new StringBuilder();
            buf.append(ch);
            i++;
            for (; i < pol.length(); i++) {
               ch = pol.charAt(i);
               if (ch >= '0' & ch <= '9') {
                  buf.append(ch);
               } else {
                  break;
               }
            }
            lStack.push(Long.parseLong(buf.toString()));
         } else {
            lStack.op("" + ch);
         }
      }
      if (lStack.plateRack.size() == 1) {
         return lStack.pop();
      } else {
         throw new RuntimeException("Parameters supplied doesn't meet requirement");
      }

   }

}
